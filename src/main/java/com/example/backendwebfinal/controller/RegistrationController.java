package com.example.backendwebfinal.controller;

import com.example.backendwebfinal.entity.User;
import com.example.backendwebfinal.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegistrationController {



    @Autowired
    private MyUserDetailsService userDetailsService;


    @ModelAttribute("user")
    public User userRegistration() {
        return new User();
    }

    @GetMapping("/registration")
    String registration(Model model) {
        model.addAttribute("users", new User());
        return "registration";
    }

    @PostMapping("/registration")
        String userRegistration(@ModelAttribute("user") User user) {
        userDetailsService.saveUser(user);
        return "redirect:/login";
    }
    // @GetMapping("/oauth2/callback/google")
    // public String googleCallback(@RequestParam("code") String code) {
    //     // Handle Google OAuth 2.0 callback
    //     // Exchange the authorization code for an access token
    //     // Redirect or perform further actions
    //     return "redirect:/oauth2/success";
    // }

    // @GetMapping("/oauth2/callback/github")
    // public String githubCallback(@RequestParam("code") String code) {
    //     // Handle GitHub OAuth 2.0 callback
    //     // Exchange the authorization code for an access token
    //     // Redirect or perform further actions
    //     return "redirect:/oauth2/success";
    // }
}
