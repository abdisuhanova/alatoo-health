package com.example.backendwebfinal.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(){
        return "login";
    }
    // @GetMapping("/login")
    // public String login() {
    //     Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    //     if (auth != null && auth.isAuthenticated()) {
    //         return "redirect:/";
    //     }
    //     return "login";
    // }

    @GetMapping("/oauth2/callback/google")
    public String googleCallback(@RequestParam("code") String code) {
        // Handle Google OAuth 2.0 callback
        // Exchange the authorization code for an access token
        // Redirect or perform further actions
        return "redirect:/oauth2/success";
    }

    @GetMapping("/oauth2/callback/github")
    public String githubCallback(@RequestParam("code") String code) {
        // Handle GitHub OAuth 2.0 callback
        // Exchange the authorization code for an access token
        // Redirect or perform further actions
        return "redirect:/oauth2/success";
    }
}
