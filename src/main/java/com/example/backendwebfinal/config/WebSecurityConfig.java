package com.example.backendwebfinal.config;

import com.example.backendwebfinal.service.MyUserDetailsService;
import com.example.backendwebfinal.service.MyUserDetailsServiceImpl;
// import com.example.backendwebfinal.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;

    // @Bean
    // public MyUserDetailsService userDetailsService() {
    //     return new MyUserDetailsServiceImpl();
    // }
    // @Autowired
    // private MyUserDetailsService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder ;
    // {
    //     return new BCryptPasswordEncoder();
    // }
    @Autowired
    private SimpleUrlAuthenticationSuccessHandler successHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    // @Bean
    // public DaoAuthenticationProvider authenticationProvider() {
    //     DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    //     authProvider.setUserDetailsService(userDetailsService());
    //     authProvider.setPasswordEncoder(passwordEncoder());

    //     return authProvider;
    // }

    // @Override
    // protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //     auth.authenticationProvider(authenticationProvider());
    // }

    // @Bean
    // public ClientRegistrationRepository clientRegistrationRepository() {
    //     return new InMemoryClientRegistrationRepository();
    // }

    // @Bean
    // public OAuth2AuthorizationRequestRedirectFilter oAuth2AuthorizationRequestRedirectFilter(ClientRegistrationRepository clientRegistrationRepository) {
    //     return new OAuth2AuthorizationRequestRedirectFilter(clientRegistrationRepository);
    // }


    // @Bean
    // public OAuth2AuthorizationRequestRepository authorizationRequestRepository() {
    //     return new HttpSessionOAuth2AuthorizationRequestRepository();
    // }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // http
        //         .csrf().disable()
        //         .authorizeRequests()
        //         .antMatchers("/", "index", "/registration", "registration", "/donation", "/css/**", "/js/**", "/img/**").permitAll()
        //         .antMatchers("/admin/**").hasAnyAuthority("ADMIN")
        //         .antMatchers("/doctor/**").hasAnyAuthority("DOCTOR")
        //         .anyRequest().authenticated()
        //         .and()
        //         .formLogin()
        //         .loginPage("/login")
        //         .successHandler(successHandler)
        //         .permitAll()
        //         .and()
        //         .logout().permitAll()
        //         .and()
        //         .exceptionHandling().accessDeniedPage("/403")
        // ;
        http
                .authorizeRequests()
                .antMatchers("/", "/index", "/registration", "/donation", "/css/**", "/js/**", "/img/**").permitAll()
                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .antMatchers("/doctor/**").hasAuthority("DOCTOR")
                .anyRequest().authenticated()
                .and()
                .oauth2Login()
                .loginPage("/login")
                .successHandler(successHandler)
                .authorizationEndpoint()
                .baseUri("/oauth2/authorize")
                .authorizationRequestRepository(authorizationRequestRepository())
                .and()
                .redirectionEndpoint()
                .baseUri("/oauth2/callback/*")
                .and()
                .userInfoEndpoint()
                .userService(userDetailsService)
            .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
            .and()
            .logout()
                .permitAll()
            .and()
            .exceptionHandling()
                .accessDeniedPage("/403")
                .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"));
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository();
    }

    @Bean
    public HttpSessionOAuth2AuthorizationRequestRepository authorizationRequestRepository() {
        return new HttpSessionOAuth2AuthorizationRequestRepository();
    }

}